<?php

namespace Rizify;

class RizifyCustomer
{
    public $fname;
    public $lname;
    public $email;
    public $address1;
    public $address2;
    public $city;
    public $state;
    public $country;
    public $ip_address;
    public $postal_code;
    public $phone;
            
    public function __construct($data)
    {
        if (empty($data))
        {
            throw new \Exception("Missing customer information");                        
        }
        
        foreach ($data as $prop => $d)
        {        
            if (property_exists($this, $prop))
            {
                $this->{$prop} = $d;
            }
            else
            {
                throw new \Exception("Property $prop does not exist on a RizifyCustomer object");
            }            
        }        
    }
}

